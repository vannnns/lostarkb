﻿open System
open System.Diagnostics
open System.Windows.Forms
open System.Runtime.InteropServices
open System.Threading
open System.Drawing
open Emgu.CV
open Emgu.CV.CvEnum
open Emgu.CV.Structure
open Emgu.CV.UI

module LostArk =

    let templateMatch (actual : Image<Gray, byte>) (template : Image<Gray, byte>) : (Point * float32) seq = 
        //let matches = new Image<Gray, byte>    
        let matches = actual.MatchTemplate(template, TemplateMatchingType.CcoeffNormed)
        
        // let mutable minVal : float = 0
        // let mutable maxVal : float = 0
        // let mutable minLoc : Point = new Point(0,0)
        // let mutable maxLoc : Point = new Point(0,0) 

        // CvInvoke.MinMaxLoc(matches, &minVal, &maxVal, &minLoc, &maxLoc)
        // maxLoc
        let threshold  = 0.8f
        seq {
            for y in 0 .. matches.Data.GetLength(0) - 1 do
                for x in 0 .. matches.Data.GetLength(1) - 1 do
                    if matches.Data[y, x, 0] >= threshold then
                        yield (new Point(x, y), matches.Data[y, x, 0])
        }

    let drawRectangle (image : Image<Bgr, byte>) (width : int) (height : int) (matche : Point)  =

        let rect = new Rectangle(matche.X, matche.Y, width, height)
        let scalar = new MCvScalar(0, 0, 255)
        CvInvoke.Rectangle(image, rect, scalar, 3)

    // [<DllImportAttribute(@"User32.dll")>]
    // extern int SetForegroundWindow(IntPtr point);


    // let p = Process.GetProcessesByName("notepad")[0];
    // let pointer = p.Handle;

    // Thread.Sleep(3000)

    //SetForegroundWindow(pointer) |> ignore
    //SendKeys.SendWait("q");

    // let template = new Image<Bgr, byte>(@"C:\Users\Vans\Documents\dev\lostarkb\data\img\found.png")
    let actual = new Image<Bgr, byte>(@"C:\Users\Vans\Documents\dev\lostarkb\data\img\bait.jpg")
    
    // let template = CvInvoke.Imread(@"C:\Users\Vans\Documents\dev\lostarkb\data\img\found.png")
    // let actual = CvInvoke.Imread(@"C:\Users\Vans\Documents\dev\lostarkb\data\img\bait.jpg")

    // let actualGray = new Mat()
    // let templateGray = new Mat()

    let actualGray = new Image<Gray, byte>(@"C:\Users\Vans\Documents\dev\lostarkb\data\img\bait.jpg")
    let templateGray = new Image<Gray, byte>(@"C:\Users\Vans\Documents\dev\lostarkb\data\img\found.png") 
    // CvInvoke.CvtColor(actual, actualGray, ColorConversion.Bgr2Gray)
    // CvInvoke.CvtColor(template, templateGray, ColorConversion.Bgr2Gray)    

    let sw = Stopwatch.StartNew()
    let results = templateMatch actualGray templateGray

    let maxResult = 
        results
        |> Seq.maxBy snd

    drawRectangle actual (templateGray.Data.GetLength 1) (templateGray.Data.GetLength 0) (fst maxResult)
    // ImageViewer.Show(actual, "Image");
    // results
    // |> Seq.iter (drawRectangle actual (templateGray.Data.GetLength 1) (templateGray.Data.GetLength 0))
    
    ImageViewer.Show(actual, "Image");

    sw.Stop()
    printfn "%i" sw.Elapsed.Milliseconds

    // For more information see https://aka.ms/fsharp-console-apps
    printfn "Hello from F#"
    Console.ReadLine() |> ignore
